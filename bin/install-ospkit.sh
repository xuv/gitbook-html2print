#!/bin/bash

# install deps
sudo pacman -S --needed --noconfirm base-devel ninja qt5


mkdir ospkit.src

# installing qtwebkit ospkit branch
cd ospkit/
git clone -o github -b ospkit --single-branch github:aleray/webkit.git
cd webkit/
WEBKIT_OUTPUTDIR=`pwd`/build/qt Tools/Scripts/build-webkit --qt --release
cd build/qt/Release/
sudo ninja install
cd ../../../../

# installing ospkit
git clone -o constant-gitlab http://gitlab.constantvzw.org/osp/tools.ospkit.git
cd tools.ospkit/src/
qmake
make
cd ../../
ln -s /tools.ospkit/src/OSPKit OSPKit
