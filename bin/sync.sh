#!/bin/bash

# @Author: Bachir Soussi Chiadmi <bach>
# @Date:   26-03-2017
# @Email:  bachir@figureslibres.io
# @Last modified by:   bach
# @Last modified time: 21-04-2017
# @License: GPL-V3

# setup
distantrepos='https://figureslibres.io/gogs/bachir/TDSM-notices.git'

# script
echo "Sync book"

# activate credential cache
git config --global credential.helper cache
git config --global credential.helper 'cache --timeout=7200'

folder='book-src'


if [ -d $folder/.git ];
then
  # if repos exists, pull
  echo "Pull $repo"
  git -C $folder pull origin master
else
  # if repos does not exists, clone
  echo "clone $repo"
  git clone $distantrepos $folder
fi
