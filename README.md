# HTML2PRINT with gitbook content management

*/!\ WORK IN PROGRESS*

## Book src

bin/sync.sh will first clone then pull books from git repositories listed in the the script into the directory book-src

## Building

bin/build.py will read the book sources and convert them to html files using pandoc (pypandoc) and BeautifulSoup

## Automation

gulp is watching assets files to convert scss to css and then rebuild html pages
gulp is also runing once on launch a webserver accessible a localhost:8000 and the bin/sync script

## dependencies
- nodejs
- python3
- css-region compatible web browser (webkit)

## use
- clone this repos
- run ```npm install```
- sync : configure the git repository url of md book in *bin/sync*
- run ```gulp``` from terminal
- configure book's main properties in assets/css/setup.scss and assets/js/setup.js
- access to your book at ```localhost:8000``` from a css-region-compatible webbrowser
- start to design your book with assets/css/styles.css
- it's also possible to add any dynamic javascript formatting with assets/js/script.js

## thanks to [osp](http://osp.kitchen/) for their [html2print boilerplate](https://gitlab.constantvzw.org/osp/tools.html2print)
